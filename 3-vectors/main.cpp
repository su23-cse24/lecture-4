#include <iostream>
#include <vector>
using namespace std;

void printVector(const vector<int>& v) {
    for (int i = 0; i < v.size(); i++) {
        cout << v[i] << endl;
    }
}

void updateVector(vector<int>& v, int index, int value) {
    v[index] = value;
}

int main() {

    /*
        Create and initialize a vector with the following integers: [1, 3, 5, 7, 9].

        Create a function that takes in the vector as a parameter and prints each item.

        Create a function that takes in the vector, an index, and a value, and updates
        the vector value at the given index
    */

    vector<int> vec = {1, 3, 5, 7, 9};

    updateVector(vec, 2, -100);

    printVector(vec);

    return 0;
}