#include <iostream>
#include <vector>
using namespace std;

void printVector(const vector<string>& v) {
    for (int i = 0; i < v.size(); i++) {
        cout << v[i] << endl;
    }
}

bool searchVector(const vector<string>& v, string target) {
    for (int i = 0; i < v.size(); i++) {
        if (v[i] == target) {
            return true;
        }
    }
    return false;
}

int main() {
    vector<string> donorList;
    vector<string> inviteList;

    int charityCount;
    cin >> charityCount;
    cin.ignore();

    for (int i = 0; i < charityCount; i++) {
        string charityName;
        getline(cin, charityName);

        int donorCount;
        cin >> donorCount;
        cin.ignore();

        for (int j = 0; j < donorCount; j++) {
            string donorName;
            getline(cin, donorName);

            if (!searchVector(donorList, donorName)) {
                donorList.push_back(donorName);
            } else {
                if (!searchVector(inviteList, donorName)) {
                    inviteList.push_back(donorName);
                }
            }
        }
    }

    printVector(inviteList);    

    return 0;
}