#include <iostream>
using namespace std;

// ['C', 'S', 'E', ' ', '2', '4', '\0']

void printString(const char str[]) {
    int i = 0;

    while (str[i] != '\0') {
        cout << str[i];
        i++;
    }
    cout << endl;
}

int main() {
    
    /* 
        Create a char array to store the string "UC Merced"

        Create a char array to store the string "CSE 24"

        Create a STL string to store "Summer 2023"

        Create a function that takes in a char array and prints every character.
        Do not pass the size of the array to the function.
    */

    char university[] = "UC Merced";

    char course[7] = {'C', 'S', 'E', ' ', '2', '4', '\0'};

    string semester = "Summer 2023";


    printString(university);
    printString(course);
    printString(semester.c_str());


    return 0;
}