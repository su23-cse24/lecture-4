#include <iostream>
using namespace std;

void printArray(int arr[], int size) {
    for (int i = 0; i < size; i++) {
        cout << arr[i] << endl;
    }
}

void updateArray(int arr[], int index, int value) {
    arr[index] = value;
}

int main() {

    /*
        Create and initialize an array with the following integers: [2, 4, 6, 8, 10].

        Create a function that takes in the array as a parameter and prints each item.

        Create a function that takes in the array, an index, and a value, and updates
        the array value at the given index
    */

    const int size = 5;
    int myArray[size] = {2, 4, 6, 8, 10};

    updateArray(myArray, 2, -100);
    
    printArray(myArray, size);

    return 0;
}